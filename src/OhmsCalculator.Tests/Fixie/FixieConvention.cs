﻿using Fixie;

namespace OhmsCalculator.Tests.Fixie
{
    class FixieConvention : Convention
    {
        public FixieConvention()
        {
            Classes.NameEndsWith("Tests");

            Methods.Where(method => method.IsVoid());
        }
    }
}
