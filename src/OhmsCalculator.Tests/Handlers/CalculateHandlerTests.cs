﻿using OhmsCalculator.Core.Enumerations;
using OhmsCalculator.Core.Features.CalculateOhms;
using Should;

namespace OhmsCalculator.Tests.Handlers
{
    public class CalculateHandlerTests
    {
        private readonly CalculateHandler _handler;

        public CalculateHandlerTests()
        {
            _handler = new CalculateHandler();
        }

        public void ShouldBe330Resistor()
        {
            var model = _handler.Handle(new CalculateQuery()
            {
                FirstBandId = ElectronicColor.Orange.Value,
                SecondBandId = ElectronicColor.Orange.Value,
                ThirdBandId = ElectronicColor.Brown.Value,
                FourthBandId = ElectronicColor.Gold.Value
            });

            model.ExactOhmValue.ShouldEqual(330);
            model.MinimumOhmValue.ShouldEqual(313.5);
            model.MaximumOhmValue.ShouldEqual(346.5);
        }
    }
}
