﻿using OhmsCalculator.Core.Enumerations;
using OhmsCalculator.Core.Features.CalculateOhms;
using Should;

namespace OhmsCalculator.Tests.Handlers
{
    public class CalculateQueryValidatorTests
    {
        private readonly CalculateQueryValidator _validator;

        public CalculateQueryValidatorTests()
        {
            _validator = new CalculateQueryValidator();
        }

        public void ValidColorCodes()
        {
            var query = new CalculateQuery()
            {
                FirstBandId = ElectronicColor.Orange.Value,
                SecondBandId = ElectronicColor.Orange.Value,
                ThirdBandId = ElectronicColor.Brown.Value,
                FourthBandId = ElectronicColor.Gold.Value
            };

            _validator.Validate(query).IsValid.ShouldBeTrue();
        }

        public void FirstBandHasWrongColor()
        {
            var query = new CalculateQuery
            {
                FirstBandId = ElectronicColor.Silver.Value,
                SecondBandId = ElectronicColor.Orange.Value,
                ThirdBandId = ElectronicColor.Brown.Value,
                FourthBandId = ElectronicColor.Gold.Value
            };

            _validator.Validate(query).Errors.Count.ShouldEqual(1);
        }

        public void SecondBandHasWrongColor()
        {
            var query = new CalculateQuery
            {
                FirstBandId = ElectronicColor.Orange.Value,
                SecondBandId = ElectronicColor.Gold.Value,
                ThirdBandId = ElectronicColor.Brown.Value,
                FourthBandId = ElectronicColor.Gold.Value
            };

            _validator.Validate(query).Errors.Count.ShouldEqual(1);
        }

        public void ThirdBandHasWrongColor()
        {
            var query = new CalculateQuery
            {
                FirstBandId = ElectronicColor.Orange.Value,
                SecondBandId = ElectronicColor.Orange.Value,
                ThirdBandId = ElectronicColor.None.Value,
                FourthBandId = ElectronicColor.Gold.Value
            };

            _validator.Validate(query).Errors.Count.ShouldEqual(1);
        }

        public void FourthBandHasWrongColor()
        {
            var query = new CalculateQuery
            {
                FirstBandId = ElectronicColor.Orange.Value,
                SecondBandId = ElectronicColor.Orange.Value,
                ThirdBandId = ElectronicColor.Brown.Value,
                FourthBandId = ElectronicColor.White.Value
            };

            _validator.Validate(query).Errors.Count.ShouldEqual(1);
        }
    }
}
