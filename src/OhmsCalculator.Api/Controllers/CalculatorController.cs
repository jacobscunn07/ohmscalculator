﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MediatR;
using OhmsCalculator.Core.Features.CalculateOhms;

namespace OhmsCalculator.Api.Controllers
{
    public class CalculatorController : ApiController
    {
        private readonly IMediator _mediator;

        public CalculatorController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // POST api/values
        public HttpResponseMessage Post(CalculateQuery query)
        {
            if (ModelState.IsValid)
            {
                var model = _mediator.Send(query);
                return Request.CreateResponse(HttpStatusCode.Accepted, model);
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }
    }
}
