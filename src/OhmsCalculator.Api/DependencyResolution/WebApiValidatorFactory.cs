﻿using System;
using System.Web.Http;
using FluentValidation;

namespace OhmsCalculator.Api.DependencyResolution
{
    public class WebApiValidatorFactory : ValidatorFactoryBase
    {
        private readonly HttpConfiguration _configuration;

        public WebApiValidatorFactory(HttpConfiguration configuration)
        {
            _configuration = configuration;
        }

        public override IValidator CreateInstance(Type validatorType)
        {
            return _configuration.DependencyResolver.GetService(validatorType) as IValidator;
        }
    }
}