﻿using System.Linq;
using FluentValidation;
using OhmsCalculator.Core.Constants;
using OhmsCalculator.Core.Enumerations;

namespace OhmsCalculator.Core.Features.CalculateOhms
{
    public class CalculateQueryValidator : AbstractValidator<CalculateQuery>
    {
        public CalculateQueryValidator()
        {
            RuleFor(x => x.FirstBandId)
                .Must(HasSignificantValue)
                .WithMessage(ErrorMessages.InvalidColor);

            RuleFor(x => x.SecondBandId)
                .Must(HasSignificantValue)
                .WithMessage(ErrorMessages.InvalidColor);

            RuleFor(x => x.ThirdBandId)
                .Must(HasMultiplierValue)
                .WithMessage(ErrorMessages.InvalidColor);

            RuleFor(x => x.FourthBandId)
                .Must(HasToleranceValue)
                .WithMessage(ErrorMessages.InvalidColor);
        }

        private bool HasSignificantValue(int colorId)
        {
            return ElectronicColor.GetSignificantValuesElectronicColors().Contains(ElectronicColor.FromValue(colorId));
        }

        private bool HasMultiplierValue(int colorId)
        {
            return ElectronicColor.GetMultiplierElectronicColors().Contains(ElectronicColor.FromValue(colorId));
        }

        private bool HasToleranceValue(int colorId)
        {
            return ElectronicColor.GetToleranceElectronicColors().Contains(ElectronicColor.FromValue(colorId));
        }
    }
}
