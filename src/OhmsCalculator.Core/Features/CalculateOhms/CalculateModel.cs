﻿namespace OhmsCalculator.Core.Features.CalculateOhms
{
    public class CalculateModel
    {
        public double MinimumOhmValue { get; set; }
        public double ExactOhmValue { get; set; }
        public double MaximumOhmValue { get; set; }
    }
}
