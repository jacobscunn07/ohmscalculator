﻿using MediatR;

namespace OhmsCalculator.Core.Features.CalculateOhms
{
    public class CalculateQuery : IRequest<CalculateModel>
    {
        public int FirstBandId { get; set; }
        public int SecondBandId { get; set; }
        public int ThirdBandId { get; set; }
        public int FourthBandId { get; set; }
    }
}
