﻿using MediatR;
using OhmsCalculator.Core.Enumerations;
using OhmsCalculator.Core.Services;

namespace OhmsCalculator.Core.Features.CalculateOhms
{
    public class CalculateHandler : IRequestHandler<CalculateQuery, CalculateModel>
    {
        public CalculateModel Handle(CalculateQuery message)
        {
            var calculatorService = new OhmValueCalculatorService();
            var calculation = calculatorService.CalculateOhmValue(ElectronicColor.FromValue(message.FirstBandId), ElectronicColor.FromValue(message.SecondBandId),
                ElectronicColor.FromValue(message.ThirdBandId), ElectronicColor.FromValue(message.FourthBandId));

            return new CalculateModel
            {
                ExactOhmValue = calculation.ExactOhmValue,
                MaximumOhmValue = calculation.MaximumOhmValue,
                MinimumOhmValue = calculation.MinimumOhmValue
            };
        }
    }
}
