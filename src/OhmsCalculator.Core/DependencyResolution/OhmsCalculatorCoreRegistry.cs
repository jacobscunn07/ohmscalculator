﻿using FluentValidation;
using MediatR;
using StructureMap.Configuration.DSL;

namespace OhmsCalculator.Core.DependencyResolution
{
    public class OhmsCalculatorCoreRegistry : Registry
    {
        public OhmsCalculatorCoreRegistry()
        {
            Scan(scanner =>
            {
                scanner.AssemblyContainingType<OhmsCalculatorCoreRegistry>();
                scanner.ConnectImplementationsToTypesClosing(typeof(IRequestHandler<,>));
                scanner.ConnectImplementationsToTypesClosing(typeof(IAsyncRequestHandler<,>));
                scanner.ConnectImplementationsToTypesClosing(typeof(INotificationHandler<>));
                scanner.ConnectImplementationsToTypesClosing(typeof(IAsyncNotificationHandler<>));
                scanner.AddAllTypesOf((typeof(IValidator<>)));
            });
            For<SingleInstanceFactory>().Use<SingleInstanceFactory>(ctx => t => ctx.GetInstance(t));
            For<MultiInstanceFactory>().Use<MultiInstanceFactory>(ctx => t => ctx.GetAllInstances(t));
            For<IMediator>().Use<Mediator>();
        }
    }
}
