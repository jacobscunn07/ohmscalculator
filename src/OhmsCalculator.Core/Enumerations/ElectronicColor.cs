﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yay.Enumerations;

namespace OhmsCalculator.Core.Enumerations
{
    public class ElectronicColor : Enumeration<ElectronicColor>
    {
        public static readonly ElectronicColor Black = new ElectronicColor(1, "Black", 0, 0, null);
        public static readonly ElectronicColor Brown = new ElectronicColor(2, "Brown", 1, 10, .01);
        public static readonly ElectronicColor Red = new ElectronicColor(3, "Red", 2, Math.Pow(10, 2), .02);
        public static readonly ElectronicColor Orange = new ElectronicColor(4, "Orange", 3, Math.Pow(10, 3), null);
        public static readonly ElectronicColor Yellow = new ElectronicColor(5, "Yellow", 4, Math.Pow(10, 4), .05);
        public static readonly ElectronicColor Green = new ElectronicColor(6, "Green", 5, Math.Pow(10, 5), .005);
        public static readonly ElectronicColor Blue = new ElectronicColor(7, "Blue", 6, Math.Pow(10, 6), .0025);
        public static readonly ElectronicColor Violet = new ElectronicColor(8, "Violet", 7, Math.Pow(10, 7), .001);
        public static readonly ElectronicColor Gray = new ElectronicColor(9, "Gray", 8, Math.Pow(10, 8), .0005);
        public static readonly ElectronicColor White = new ElectronicColor(10, "White", 9, Math.Pow(10, 9), null);
        public static readonly ElectronicColor Gold = new ElectronicColor(11, "Gold", null, Math.Pow(10, -1), .05);
        public static readonly ElectronicColor Silver = new ElectronicColor(12, "Silver", null, Math.Pow(10, -2), .10);
        public static readonly ElectronicColor None = new ElectronicColor(13, "None", null, null, .20);

        public ElectronicColor(int value, string displayName, int? significantFigures, double? multiplier, double? tolerance) 
            : base(value, displayName)
        {
            SignificantFigures = significantFigures;
            Multiplier = multiplier;
            Tolerance = tolerance;
        }

        public int? SignificantFigures { get; private set; }
        public double? Multiplier { get; private set; }
        public double? Tolerance { get; private set; }

        public static IEnumerable<ElectronicColor> GetElectronicColors()
        {
            return new[]
            {
                Black, Brown, Red, Orange, Yellow, Green, Blue, Violet, Gray, White, Gold, Silver, None
            };
        }

        public static IEnumerable<ElectronicColor> GetSignificantValuesElectronicColors()
        {
            return GetElectronicColors().Where(x => x.SignificantFigures != null);
        }

        public static IEnumerable<ElectronicColor> GetMultiplierElectronicColors()
        {
            return GetElectronicColors().Where(x => x.Multiplier != null);
        }

        public static IEnumerable<ElectronicColor> GetToleranceElectronicColors()
        {
            return GetElectronicColors().Where(x => x.Tolerance != null);
        }
    }
}
