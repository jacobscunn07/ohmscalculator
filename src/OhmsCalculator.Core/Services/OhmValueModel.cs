﻿namespace OhmsCalculator.Core.Services
{
    public class OhmValueModel
    {
        public double MinimumOhmValue { get; set; }
        public double ExactOhmValue { get; set; }
        public double MaximumOhmValue { get; set; }
    }
}
