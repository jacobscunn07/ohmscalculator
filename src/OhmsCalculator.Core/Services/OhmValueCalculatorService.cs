﻿using System;
using OhmsCalculator.Core.Enumerations;

namespace OhmsCalculator.Core.Services
{
    public class OhmValueCalculatorService : IOhmValueCalculator
    {
        public OhmValueModel CalculateOhmValue(ElectronicColor firstBand, ElectronicColor secondBand, ElectronicColor thirdBand,
            ElectronicColor fourthBand)
        {
            if (firstBand.SignificantFigures == null ||
                secondBand.SignificantFigures == null ||
                thirdBand.Multiplier == null ||
                fourthBand.Tolerance == null)
                throw new Exception("Invalid electronic color selection");

            var number =
                Convert.ToInt32($"{firstBand.SignificantFigures}{secondBand.SignificantFigures}");
            var ohm = number * thirdBand.Multiplier;
            var maxOhm = ohm + ohm*fourthBand.Tolerance;
            var minOhm = ohm - ohm*fourthBand.Tolerance;

            return new OhmValueModel
            {
                MinimumOhmValue = (double)minOhm,
                ExactOhmValue = (double)ohm,
                MaximumOhmValue = (double)maxOhm

            };
        }
    }
}
