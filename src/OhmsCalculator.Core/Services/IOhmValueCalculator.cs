﻿using OhmsCalculator.Core.Enumerations;

namespace OhmsCalculator.Core.Services
{
    interface IOhmValueCalculator
    {
        OhmValueModel CalculateOhmValue(ElectronicColor firstBand, ElectronicColor secondBand, ElectronicColor thirdBand,
            ElectronicColor fourthBand);
    }
}
