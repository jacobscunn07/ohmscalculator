﻿require(['/Scripts/app/main.js'], function () {
    require(['select2'], function () {
        $(function () {
            require(['modelData'], function (modelData) {
                function formatColor(color) {
                    var $color = $(
                      '<span><div class="select2-box" style="background-color:' + color.text + '"></div>' + color.text + '</span>'
                    );
                    return $color;
                };

                var select2Options = {
                    templateResult: formatColor,
                    templateSelection: formatColor
                };

                $("#firstColorBand").select2(select2Options);
                $("#secondColorBand").select2(select2Options);
                $("#thirdColorBand").select2(select2Options);
                $("#fourthColorBand").select2(select2Options);

                $("#submit").click(function () {
                    var spinner = $("<i class='fa fa-spinner fa-spin'></i>");
                    $("#submit").append(spinner);
                    $("#submit").attr("disabled", true);
                    var data = {
                        FirstBandId: $("#firstColorBand").val(),
                        SecondBandId: $("#secondColorBand").val(),
                        ThirdBandId: $("#thirdColorBand").val(),
                        FourthBandId: $("#fourthColorBand").val()
                    }

                    $.ajax({
                        type: "POST",
                        url: "http://127.0.0.1:8082/api/calculator",
                        data: data,
                        success: function (data) {
                            $("#exactOhmValue").text(data.ExactOhmValue + " ohms");
                            $("#minimumOhmValue").text(data.MinimumOhmValue + " ohms");
                            $("#maximumOhmValue").text(data.MaximumOhmValue + " ohms");
                            $("#results").css({ visibility: "visible" });
                            $("#submit").attr("disabled", false);
                            $(spinner).remove();
                        }
                    });
                });
            });
        });
    });
});