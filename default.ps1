Framework "4.5.2"

$projectName = "OhmsCalculator"
$configuration = 'Debug'
$basedir = resolve-path '.\'
$src = "$basedir\src"
$sln = "$src\OhmsCalculator.sln"
$ipAddress = "127.0.0.1"

task default -depends RunTests

task Compile {
    exec { msbuild /t:clean /v:q /nologo /p:Configuration=$configuration $sln }
    exec { msbuild /t:build /v:q /nologo /p:Configuration=$configuration $sln }
}

task RunTests -depends Compile {
    $fixieRunners = @(gci $src\packages -rec -filter Fixie.Console.exe)

    if ($fixieRunners.Length -ne 1) {
        throw "Expected to find 1 Fixie.Console.exe, but found $($fixieRunners.Length)."
    }

    $fixieRunner = $fixieRunners[0].FullName

    exec { & $fixieRunner "$src\OhmsCalculator.Tests\bin\$configuration\OhmsCalculator.Tests.dll" }
}

task CreateSite {
	New-Website -Name "OhmsCalculator.Api" -IPAddress "$ipAddress" -Port "8082" -PhysicalPath "$src\OhmsCalculator.Api" -ApplicationPool ".NET v4.5" -Force | Out-Null
}

task DeleteSite {
	Remove-Website -Name "OhmsCalculator.Api" | Out-Null
}
