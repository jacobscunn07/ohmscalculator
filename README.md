### What is this repository for? ###

Technical assessment for [blinds.com](http://www.blinds.com)

### How do I get set up? ###

* In windows file explorer, navigate to root of solution
* Open Command Prompt as Administrator
* Run command: build
* Run command: build CreateSite (note: this will create a website on your local iis at http://127.0.0.1:8082/)